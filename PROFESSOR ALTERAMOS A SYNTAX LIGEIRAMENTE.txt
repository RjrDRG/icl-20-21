
#######################################################################################################################
booleanos syntax

def m: bool = false in ... end;;
def m: bool = true in ... end;;

#######################################################################################################################
referencias syntax

Ao criar um objecto com operador new é possivel especificar o tipo do objecto:    new int 2;;  new bool true;;  new bool false;;
Caso não seja especificado o compilador assume que o tipo é um inteiro:           new 2;;
new true;; e new false;; dao um erro de compilação.

___________________________________________________
def m:ref int = new 2 in ... end;;
def m:ref int = new int 2 in ... end;;

ambas as expressões acima são equivalentes
___________________________________________________

def m:ref bool = new bool false in ... end;;