
.class public Main
.super java/lang/Object

;standard initializer

.method public <init>()V
    aload_0
    invokenonvirtual java/lang/Object/<init>()V
    return
.end method

.method public static main([Ljava/lang/String;)V

    ; set limits used by this method
    .limit locals 2
    .limit stack 256

    ; setup local variables:
    ; 0 - spaghetti stack data structure
    ; 1 - result

    ; place your bytecodes here:

    ; START

BYTECODES

    ; END

    istore_1
    getstatic java/lang/System/out Ljava/io/PrintStream;
    iload_1
    invokevirtual java/io/PrintStream/println(I)V

    return

.end method