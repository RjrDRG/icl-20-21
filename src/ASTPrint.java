import exceptions.TypeErrorException;

public class ASTPrint implements ASTNode {

    ASTNode p;

    public IValue eval(Environment<IValue> e) {
        System.out.print(p.eval(e).toString());
        return new VEmpty();
	}
    
    public void compile(CodeBlock c, CompilerEnvironment e) {
       c.emit("getstatic java/lang/System/out Ljava/io/PrintStream;");
       p.compile(c, e);
       c.emit("invokevirtual java/io/PrintStream/print(I)V");
    }

    public IType typecheck(Environment<IType> e) {
        return p.typecheck(e);
    }

    public ASTPrint(ASTNode p) {
        this.p = p;
    }
}