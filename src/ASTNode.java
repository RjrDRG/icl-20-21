public interface ASTNode {
    IValue eval(Environment<IValue> e);
    void compile(CodeBlock c, CompilerEnvironment e);
    IType typecheck(Environment<IType> e);
}

