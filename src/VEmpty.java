class VEmpty implements IValue{

	public VEmpty(){
	
	}

	public boolean equals(IValue other){
		return other instanceof VEmpty;
	}
	
	public String toString(){
		return "";
	}

}