import java.util.ArrayList;
import java.util.List;

public class RefBool {

    public static String getCode() {
        List<String> code = new ArrayList<>();

        code.add(".class public ref_bool");
        code.add(".super java/lang/Object");
        code.add(".field public value I");
        code.add(".method public <init>()V");
        code.add("\taload_0");
        code.add("\tinvokenonvirtual java/lang/Object/<init>()V");
        code.add("\treturn");
        code.add(".end method");

        return String.join(System.lineSeparator(), code);
    }
}