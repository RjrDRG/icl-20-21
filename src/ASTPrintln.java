import exceptions.TypeErrorException;

public class ASTPrintln implements ASTNode {

    ASTNode p;

    public IValue eval(Environment<IValue> e) {
        System.out.println(p.eval(e).toString());
        return new VEmpty();
	}
    
    public void compile(CodeBlock c, CompilerEnvironment e) {
       c.emit("getstatic java/lang/System/out Ljava/io/PrintStream;");
       if(p instanceof ASTEmpty){
         c.emit("invokevirtual java/io/PrintStream/println()V");
       }
       else {
         p.compile(c, e);
         c.emit("invokevirtual java/io/PrintStream/println(I)V");
       }
    }

    public IType typecheck(Environment<IType> e) {
        return p.typecheck(e);
    }

    public ASTPrintln(ASTNode p) {
        this.p = p;
    }
}