import exceptions.IdentifierAlreadyDeclaredException;
import exceptions.UndeclaredIdentifierException;

import java.util.HashMap;
import java.util.Map;

public class CompilerEnvironment {

    private CompilerEnvironment parent;
    private Map<String,IType> vars;

    private Integer level;
    private Integer sublevel;
    private int childCount;

    public CompilerEnvironment() {
        parent = null;
        vars = null;
        level = null;
        sublevel = null;
        childCount = 0;
    }

    public CompilerEnvironment(CompilerEnvironment parent) {
        this.parent = parent;
        vars = new HashMap<>();
        level = parent.level != null ? parent.level + 1 : 0;
        sublevel = parent.childCount;
        childCount = 0;
        parent.childCount = parent.childCount + 1;
    }

    public CompilerEnvironment beginScope() {
        return new CompilerEnvironment(this);
    }

    public CompilerEnvironment endScope(){
        return parent;
    }

    public void assoc(String varId, IType t){
        vars.put(varId, t);
    }

    public int find(String varId) {
        if(vars.containsKey(varId))
            return level;
        else if(parent != null)
            return parent.find(varId);
        else
            throw new UndeclaredIdentifierException(varId);
    }
    
    public IType getType(String varId) {
        return vars.get(varId);
    }

    public int getLevel() {
        return level;
    }

    public CompilerEnvironment getParent() {
        return parent;
    }

    public String getCurrentFrameType() {
        return "frame_" + level + "_" + sublevel;
    }

    public String getPreviousFrameType() {
        if(level > 0)
            return "frame_" + parent.level + "_" + parent.sublevel;
        else
            return "java/lang/Object";
    }

    public Frame toFrame() {
        return new Frame(getCurrentFrameType(), getPreviousFrameType(), vars);
    }
}
