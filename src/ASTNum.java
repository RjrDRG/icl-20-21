public class ASTNum implements ASTNode {

    int val;

    public IValue eval(Environment<IValue> e) {
        return new VInt(val);
    }

    public void compile(CodeBlock c, CompilerEnvironment e) {
        c.emit("sipush " + val);
    }
    
    public IType typecheck(Environment<IType> e) { 
            return new TInt();
    }

    public ASTNum(int n) {
        val = n;
    }

}

