import exceptions.TypeErrorException;

public class ASTIf implements ASTNode {

    ASTNode if_;
    ASTNode then_;
    ASTNode else_;

    public IValue eval(Environment<IValue> e) {
        IValue i = if_.eval(e);
        if(i instanceof VBool) {
            if (((VBool) i).getval()) {
                return then_.eval(e);
            } else {
                return else_.eval(e);
            }
        }

        throw new TypeErrorException("ASTIf: argument is not a boolean");
	}

    
    public void compile(CodeBlock c, CompilerEnvironment e) {
       int i_if = c.incrementIfCount();
       
       if(else_ instanceof ASTEmpty){
           if_.compile(c, e);
           c.emit("ifeq IfExit" + i_if);
           then_.compile(c, e);
           c.emit("IfExit" + i_if + ":");
       }else{
           if_.compile(c, e);
           c.emit("ifeq IfElse" + i_if);
           then_.compile(c, e);
           c.emit("goto IfExit" + i_if);
           c.emit("IfElse" + i_if + ":");
           else_.compile(c, e);
           c.emit("IfExit" + i_if + ":");
       }
    }

    public IType typecheck(Environment<IType> e) {
        IType t1 = if_.typecheck(e);
        if(t1 instanceof TBool) {
            IType t2 = then_.typecheck(e);
            IType t3 = else_.typecheck(e);
            if (t2.getClass().equals(t3.getClass()) || t3 instanceof TEmpty) {
                return t2;
            }
        }

        throw new TypeErrorException("ASTIf: branch types mismatch");
    }

    public ASTIf(ASTNode if_, ASTNode then_, ASTNode else_ ) {
        this.if_ = if_;
        this.else_ = else_;
        this.then_ = then_;
    }
}