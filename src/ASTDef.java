import exceptions.IdentifierAlreadyDeclaredException;

import java.util.LinkedHashMap;
import java.util.Map;

public class ASTDef implements ASTNode {
    LinkedHashMap<String, ASTNode> vars;
    LinkedHashMap<String, IType> varsTypes;
    ASTNode body;

    public ASTDef() {
        vars = new LinkedHashMap<>();
        varsTypes = new LinkedHashMap<>();
    }

    public void addVar(String id, ASTNode value, IType te) {
        ASTNode old = vars.put(id, value);
        varsTypes.put(id, te);

        if(old != null)
            throw new IdentifierAlreadyDeclaredException(id);
    }

    public ASTNode close(ASTNode body) {
        this.body = body;
        return this;
    }

    public IValue eval(Environment<IValue> e) {
        e = e.beginScope();

        for(Map.Entry<String,ASTNode> var : vars.entrySet()) { //LinkedHashMap guaranties order of insertion
            e.assoc(var.getKey(), var.getValue().eval(e));
        }

        IValue val = body.eval(e);
        e.endScope();
        return val;
    }
    
     public void compile(CodeBlock c, CompilerEnvironment e) { 
        e = e.beginScope();
        c.emit("new " + e.getCurrentFrameType());
        c.emit("dup");
        c.emit("invokespecial " + e.getCurrentFrameType() + "/<init>()V");
        c.emit("dup");
        c.emit("aload_0");
        c.emit("putfield " + e.getCurrentFrameType() + "/sl L" + e.getPreviousFrameType() + ";");
        c.emit("dup");
        c.emit("astore_0");
        for(Map.Entry<String, ASTNode> var : vars.entrySet()){
            IType type = varsTypes.get(var.getKey());
            e.assoc(var.getKey(), type);
            c.emit("dup");
            var.getValue().compile(c, e);
            if( type instanceof TInt || type instanceof TBool){
                c.emit("putfield " + e.getCurrentFrameType() + "/" + var.getKey() + " I;");
            }
            else {
                c.emit("putfield " + e.getCurrentFrameType() + "/" + var.getKey() + " Ljava/lang/Object;");
            }
        }
        c.emit("pop");
        c.save(e.toFrame());

        body.compile(c, e);

        c.emit("aload_0");
        c.emit("getfield " + e.getCurrentFrameType() + "/sl" + " L" + e.getPreviousFrameType() + ";");
        c.emit("astore_0");
        e.endScope();
   }
   
   public IType typecheck(Environment<IType> e) { 
        e = e.beginScope();
        
        for(Map.Entry<String,IType> var : varsTypes.entrySet()) { //LinkedHashMap guaranties order of insertion
            e.assoc(var.getKey(), var.getValue());
        }

        IType t = body.typecheck(e);
        e.endScope();
        return t;
    }      
}