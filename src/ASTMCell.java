import exceptions.TypeErrorException;

public class ASTMCell implements ASTNode {

    ASTNode n;
    IType t;

    public IValue eval(Environment<IValue> e) {
       return n.eval(e);
    }

    public void compile(CodeBlock c, CompilerEnvironment e) {
        if(t instanceof TInt){
            c.emit("new ref_int");
            c.emit("dup");
            c.emit("invokespecial ref_int/<init>()V");
            c.emit("dup");
            n.compile(c,e);
            c.emit("putfield ref_int/value I");
        }
        else{
            c.emit("new ref_bool");
            c.emit("dup");
            c.emit("invokespecial ref_bool/<init>()V");
            c.emit("dup");
            n.compile(c,e);
            c.emit("putfield ref_bool/value I");
        }
    }
    
    public IType typecheck(Environment<IType> e) {
        IType te = n.typecheck(e);
        
        if(te.getClass().equals(t.getClass()) && (t instanceof TBool || t instanceof TInt)) {
            return new TRef(t);
        }
        else throw new TypeErrorException("ASTMCell: argument is not an integer or a boolean");
    }
    
    public IType getType() {
        return t;
    }

    public ASTMCell(ASTNode n, IType t) {
        this.n = n;
        this.t = t;
    }
}