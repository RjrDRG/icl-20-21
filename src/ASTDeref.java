import exceptions.TypeErrorException;

public class ASTDeref implements ASTNode {

    ASTNode ref;

    public IValue eval(Environment<IValue> e) {
        return ref.eval(e);
	}
    
    public void compile(CodeBlock c, CompilerEnvironment e) {
        ref.compile(c, e);
    
        IType type;
        if(ref instanceof ASTId) {
           String varId = ((ASTId)ref).getId();
           type = ((TRef)e.getType(varId)).getRefType();
        }
        else {
           type = ((ASTMCell)ref).getType();
        }

        if(type instanceof TInt){
            c.emit("getfield ref_int/value I");
        } else{
            c.emit("getfield ref_bool/value I");
        }
    }
    
    public IType typecheck(Environment<IType> e) {
        IType t = ref.typecheck(e);
        if (t instanceof TRef) { 
            return ((TRef)t).getRefType();
        }
        else throw new TypeErrorException("ASTDeref: argument is not a memory cell: " + t.toString());
    }

    public ASTDeref(ASTNode ref) {
        if(ref instanceof ASTMCell || ref instanceof ASTId){
			this.ref = ref;
		}
		else throw new TypeErrorException("ASTDeref: argument is not a memory cell");
    }
}