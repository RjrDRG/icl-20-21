import exceptions.TypeErrorException;

public class ASTSeq implements ASTNode {

    ASTNode lhs, rhs;

    public IValue eval(Environment<IValue> e) {
        lhs.eval(e);
        return rhs.eval(e);
    }

    public void compile(CodeBlock c, CompilerEnvironment e) {
        lhs.compile(c, e);
        c.emit("pop");
        rhs.compile(c, e);
    }

    public IType typecheck(Environment<IType> e) {
        lhs.typecheck(e);
        return rhs.typecheck(e);
    }

    public ASTSeq(ASTNode l, ASTNode r) {
        lhs = l;
        rhs = r;
    }
}

