import exceptions.TypeErrorException;

public class ASTNot implements ASTNode {

    ASTNode node;

    public IValue eval(Environment<IValue> e) {
        IValue v = node.eval(e);
        if(v instanceof VBool){
            return ((VBool)v).not();
        }
        throw new TypeErrorException("ASTNot: argument is not a boolean");
    }

    public void compile(CodeBlock c, CompilerEnvironment e) {
        node.compile(c, e);
        c.emit("sipush 1");
        c.emit("isub");
        c.emit("dup");
        c.emit("imul");
    }

    public IType typecheck(Environment<IType> e) { 
        IType t = node.typecheck(e);
        if (t instanceof TBool) { 
            return new TBool();
        }
        else throw new TypeErrorException("ASTNot: argument is not a boolean");
    }
    
    public ASTNot(ASTNode node) {
        this.node = node;
    }
}

