import exceptions.TypeErrorException;

public class ASTOr implements ASTNode {

    ASTNode lhs, rhs;

    public IValue eval(Environment<IValue> e) {
        IValue v1 = lhs.eval(e);
        IValue v2 = rhs.eval(e);
        if(v1 instanceof VBool && v2 instanceof VBool){
            return ((VBool)v1).or((VBool)v2);
        }

        throw new TypeErrorException("ASTOr: argument is not a boolean");
    }

    public void compile(CodeBlock c, CompilerEnvironment e) {
        lhs.compile(c, e);
        rhs.compile(c, e);
        c.emit("ior");
    }

    public IType typecheck(Environment<IType> e) { 
        IType t1 = lhs.typecheck(e);
        IType t2 = rhs.typecheck(e);
        if (t1 instanceof  TBool && t2 instanceof  TBool) { 
            return new TBool();
        }
        else throw new TypeErrorException("ASTOr: argument is not a boolean");
    }

    public ASTOr(ASTNode l, ASTNode r) {
        lhs = l;
        rhs = r;
    }
}

