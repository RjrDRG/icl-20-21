class VBool implements IValue{
	private boolean b;

	public VBool(boolean b){
	this.b = b;
	}

	public boolean getval(){
	return b;
	}

	public boolean equals(IValue other){
		if(other instanceof VBool){
			return b == ((VBool)other).getval();
		}
		return false;
	}

	public VBool and(VBool other) {
		return new VBool(b && other.getval());
	}

	public VBool or(VBool other) {
		return new VBool(b || other.getval());
	}

	public VBool not() {
		return new VBool(!b);
	}
	
	public String toString(){
		return ((Boolean)b).toString();
	}

}