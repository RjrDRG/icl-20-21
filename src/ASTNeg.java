import exceptions.TypeErrorException;

public class ASTNeg implements ASTNode {

    ASTNode node;

    public IValue eval(Environment<IValue> e) {
        IValue v = node.eval(e);
        if (v instanceof VInt) {
            return new VInt(-((VInt) v).getval());
        }
        throw new TypeErrorException("ASTNeg: argument is not an integer");
    }
    
    public void compile(CodeBlock c, CompilerEnvironment e) {
        node.compile(c, e);
        c.emit("ineg");
    }
    
    public IType typecheck(Environment<IType> e) { 
        IType t = node.typecheck(e);
        if (t instanceof TInt) { 
            return new TInt();
        }
        else throw new TypeErrorException("ASTEquals: argument is not an integer");
    }

    public ASTNeg(ASTNode n) {
        node = n;
    }

}