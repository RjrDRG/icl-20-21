import exceptions.TypeErrorException;

public class ASTGreater implements ASTNode {

    ASTNode lhs, rhs;

    public IValue eval(Environment<IValue> e) {
        IValue v1 = lhs.eval(e);
        if(v1 instanceof VInt){
            IValue v2 = rhs.eval(e);
            if(v2 instanceof VInt){
                return new VBool(((VInt)v1).getval() > ((VInt)v2).getval());
            }
        }

        throw new TypeErrorException("ASTGreater: argument is not an integer");
    }

    public void compile(CodeBlock c, CompilerEnvironment e) {
        lhs.compile(c, e);
        rhs.compile(c, e);
        int cl = c.getCurrentLineNumber();
        c.emit("if_icmpgt L" + (cl + 3));
        c.emit("iconst_0");
        c.emit("goto L" + (cl + 5));
        c.emit("L" + c.getCurrentLineNumber() + ":");
        c.emit("iconst_1");
        c.emit("L" + c.getCurrentLineNumber() + ":");
    }
    
    public IType typecheck(Environment<IType> e) { 
        IType t1 = lhs.typecheck(e);
        IType t2 = rhs.typecheck(e);
        if (t1 instanceof  TInt && t2 instanceof  TInt) { 
            return new TBool();
        }
        else throw new TypeErrorException("ASTGreater: argument is not an integer");
    }
    

    public ASTGreater(ASTNode l, ASTNode r) {
        lhs = l;
        rhs = r;
    }
}

