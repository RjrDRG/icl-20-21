import exceptions.TypeErrorException;

public class ASTEquals implements ASTNode {

    ASTNode lhs, rhs;

    public IValue eval(Environment<IValue> e) {
        IValue v1 = lhs.eval(e);
        IValue v2 = rhs.eval(e);
        return new VBool(v1.equals(v2));
    }

    public void compile(CodeBlock c, CompilerEnvironment e) {
        lhs.compile(c, e);
        rhs.compile(c, e);
        int cl = c.getCurrentLineNumber();
        c.emit("if_icmpeq L" + (cl + 3));
        c.emit("iconst_0");
        c.emit("goto L" + (cl + 5));
        c.emit("L" + c.getCurrentLineNumber() + ":");
        c.emit("iconst_1");
        c.emit("L" + c.getCurrentLineNumber() + ":");
    }
    
    public IType typecheck(Environment<IType> e) { 
        IType t1 = lhs.typecheck(e);
        IType t2 = rhs.typecheck(e);
        if (t1 instanceof TInt && t2 instanceof TInt) { 
            return new TBool();
        }
        else throw new TypeErrorException("ASTEquals: argument is not an integer");
    }

    public ASTEquals(ASTNode l, ASTNode r) {
        lhs = l;
        rhs = r;
    }
}

