public class VInt implements IValue {
    private int v;
    
    public VInt(int v) {
        this.v = v;
    }

    public int getval() {
        return v;
    }

    public IValue add(VInt other){
        return new VInt(v + other.getval());
    }

    public IValue sub(VInt other){
        return new VInt(v - other.getval());
    }

    public IValue times(VInt other){
        return new VInt(v * other.getval());
    }

    public IValue div(VInt other){
        return new VInt(v / other.getval());
    }

    public IValue mod(VInt other){
        return new VInt(v % other.getval());
    }


    public boolean equals(IValue other){
        if(other instanceof VInt){
            return v == ((VInt)other).getval();
        }
        return false;
    }
    
    public String toString(){
        return ((Integer)v).toString();
    }
    
}