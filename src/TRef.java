public class TRef implements IType {

  private IType tr; 
  
  public TRef(IType t) { tr = t; } 
  
  public IType getRefType() { return tr;}

  public String toString() {
    return "TRef_" + tr.toString();
  }
}