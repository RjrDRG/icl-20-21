import exceptions.TypeErrorException;

public class ASTDiv implements ASTNode {

    ASTNode lhs, rhs;

    public IValue eval(Environment<IValue> e) {
        IValue v1 = lhs.eval(e);
        if(v1 instanceof VInt){
            IValue v2 = rhs.eval(e);
            if(v2 instanceof VInt){
                return ((VInt)v1).div((VInt)v2);
            }
        }

        throw new TypeErrorException("ASTDiv: argument is not an integer");
    }
    
    public void compile(CodeBlock c, CompilerEnvironment e) {
        lhs.compile(c, e);
        rhs.compile(c, e);
        c.emit("idiv");
    }
    
    public IType typecheck(Environment<IType> e) { 
        IType t1 = lhs.typecheck(e);
        IType t2 = rhs.typecheck(e);
        if (t1 instanceof TInt && t2 instanceof TInt) { 
            return new TInt();
        }
        else throw new TypeErrorException("ASTDiv: argument is not an integer");
    }

    public ASTDiv(ASTNode l, ASTNode r) {
        lhs = l;
        rhs = r;
    }
}