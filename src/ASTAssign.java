import exceptions.TypeErrorException;

public class ASTAssign implements ASTNode {

    ASTNode mcell;
    ASTNode value;

    public IValue eval(Environment<IValue> e) {
        IValue v = value.eval(e);
        if(mcell instanceof ASTId) {
            String id = ((ASTId) mcell).getId();
            e.reassoc(id,v);
        }
        return v;
    }

    public void compile(CodeBlock c, CompilerEnvironment e) { 
        IType type;
        if(mcell instanceof ASTId) {
           String varId = ((ASTId)mcell).getId();
           type = e.getType(varId);
        }
        else {
           type = ((ASTMCell)mcell).getType();
        }

        value.compile(c, e);
        c.emit("dup");
        mcell.compile(c, e);
        c.emit("swap");
        if(type instanceof TInt){
            c.emit("putfield ref_int/value I");
        } else{
            c.emit("putfield ref_bool/value I");
        }
    }
    
    public IType typecheck(Environment<IType> e) { 
        IType t1 = mcell.typecheck(e);
        IType t2 = value.typecheck(e);
        
        if (t1 instanceof TRef) {
            if(t2 instanceof TInt || t2 instanceof TBool) {
                if (mcell instanceof ASTId) {
                    e.reassoc(((ASTId) mcell).getId(), new TRef(t2));
                }
                return new TEmpty();
            }
            else throw new TypeErrorException("ASTAssign: argument is not a integer or boolean value ");
        }
        else throw new TypeErrorException("ASTAssign: argument is not a memory cell ");
    }

    public ASTAssign(ASTNode l, ASTNode r) {
        if(l instanceof ASTMCell || l instanceof ASTId) {
            mcell = l;
            value = r;
        }
        else throw new TypeErrorException("ASTAssign: argument is not a memory cell");
    }
}

