import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Frame {
    private String type;
    private String parentType;
    private Map<String, IType> vars;

    public Frame(String type, String parentType, Map<String, IType> vars) {
        this.type = type;
        this.parentType = parentType;
        this.vars = vars;
    }

    public String getName() {
        return type;
    }

    public String getCode() {
        List<String> code = new ArrayList<>();

        code.add(".class public " + type);
        code.add(".super java/lang/Object");

        code.add(".field public sl L" + parentType + ";");

        for(Map.Entry<String, IType> var : vars.entrySet()) {
            String stype;
            if(var.getValue() instanceof TRef) stype = "Ljava/lang/Object";
            else stype = "I";
            code.add(".field public " + var.getKey() + " " + stype);
        }

        code.add(".method public <init>()V");
        code.add("\taload_0");
        code.add("\tinvokenonvirtual java/lang/Object/<init>()V");
        code.add("\treturn");
        code.add(".end method");

        return String.join(System.lineSeparator(), code);
    }
}
