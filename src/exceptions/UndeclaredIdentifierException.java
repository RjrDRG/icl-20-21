package exceptions;

public class UndeclaredIdentifierException extends RuntimeException {
    public UndeclaredIdentifierException(String s){
        super(s);
    }
}
