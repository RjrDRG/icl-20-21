package exceptions;

public class IdentifierAlreadyDeclaredException extends RuntimeException {
    public IdentifierAlreadyDeclaredException(String s){
        super(s);
    }
}
