package exceptions;

public class TypeErrorException extends RuntimeException {
    public TypeErrorException(String s){
        super(s);
    }
}
