import exceptions.TypeErrorException;

public class ASTAnd implements ASTNode {

    ASTNode lhs, rhs;

    public IValue eval(Environment<IValue> e) {
        IValue v1 = lhs.eval(e);
        IValue v2 = rhs.eval(e);
        if(v1 instanceof VBool && v2 instanceof VBool){
            return ((VBool)v1).and((VBool)v2);
        }
        else throw new TypeErrorException("ASTAnd: argument is not a boolean");
    }

    public void compile(CodeBlock c, CompilerEnvironment e) {
        lhs.compile(c, e);
        rhs.compile(c, e);
        c.emit("iand");
    }
    
    public IType typecheck(Environment<IType> e) { 
        IType t1 = lhs.typecheck(e);
        IType t2 = rhs.typecheck(e);
        if (t1 instanceof  TBool && t2 instanceof  TBool) { 
            return new TBool();
        }
        else throw new TypeErrorException("ASTAnd: argument is not a boolean");
    }
    
    public ASTAnd(ASTNode l, ASTNode r) {
        lhs = l;
        rhs = r;
    }
}

