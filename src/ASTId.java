public class ASTId implements ASTNode {
    private String id;

    public IValue eval(Environment<IValue> e) {
        return e.find(id);
    }
    
    public void compile(CodeBlock c, CompilerEnvironment e) { 
        int targetLevel = e.find(id);
        c.emit("aload_0");

        CompilerEnvironment currentEnvironment = e;
        while(currentEnvironment.getLevel() > targetLevel){
            c.emit("getfield " + currentEnvironment.getCurrentFrameType() + "/sl L" + currentEnvironment.getPreviousFrameType() + ";");
            currentEnvironment = currentEnvironment.getParent();
        }

        IType type = e.getType(id);

        if( type instanceof TInt || type instanceof TBool ){
            c.emit("getfield " + currentEnvironment.getCurrentFrameType() + "/" + id + " I;");
        } 
        else {
            c.emit("getfield " + currentEnvironment.getCurrentFrameType() + "/" + id + " Ljava/lang/Object;");
            if(((TRef)type).getRefType() instanceof TInt) {
                c.emit("checkcast ref_int");
            }
            else {
                c.emit("checkcast ref_bool");
            }
        }
    }
    
    public IType typecheck(Environment<IType> e) { 
        return e.find(id);
    }

    public String getId() {
        return id;
    }
    
    public ASTId(String id) {
        this.id = id;
    }
}
