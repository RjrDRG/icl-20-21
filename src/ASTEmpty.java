import exceptions.TypeErrorException;

public class ASTEmpty implements ASTNode {

    public IValue eval(Environment<IValue> e) {
       return new VEmpty();
    }

    public void compile(CodeBlock c, CompilerEnvironment e) {
        c.emit("iconst_0");
    }

    public IType typecheck(Environment<IType> e) {
        return new TEmpty();
    }

    public ASTEmpty() {
    }
}

