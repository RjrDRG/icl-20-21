public class ASTBool implements ASTNode {

    boolean val;

    public IValue eval(Environment<IValue> e) {
        return new VBool(val);
    }

    public void compile(CodeBlock c, CompilerEnvironment e) {
        if(val) c.emit("iconst_1");
        else c.emit("iconst_0");
    }
    
    public IType typecheck(Environment<IType> e) { 
        return new TBool();
    }

    public ASTBool(boolean b) {
        val = b;
    }

}

