public class VMCell implements IValue {
    private IValue v;
    
    public VMCell(IValue v) {
        this.v = v;
    }
    
    public IValue get() {
        return v;
    }
    
    public void set(IValue v) {
        this.v = v;
    }

    public boolean equals(IValue other){
        return this == other;
    }
    
    public String toString(){
        return v.toString();
    }
}