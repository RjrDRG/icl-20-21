import exceptions.IdentifierAlreadyDeclaredException;
import exceptions.UndeclaredIdentifierException;

import java.util.HashMap;
import java.util.Map;

public class Environment<T> {

    private Environment<T> parent;
    private Map<String,T> defs;

    public Environment() {
        this(null);
    }

    public Environment(Environment<T> parent) {
        this.parent = parent;
        defs = new HashMap<>();
    }

    public Environment<T> beginScope() {
        return new Environment<T>(this);
    }

    public Environment<T> endScope(){
        return parent;
    }

    public void assoc(String id, T val){
        if(defs.containsKey(id))
            throw new IdentifierAlreadyDeclaredException(id);
        else
            defs.put(id,val);
    }

    public void reassoc(String id, T val){
        defs.put(id,val);
    }

    public T find(String id) {
        if(defs.containsKey(id))
            return defs.get(id);
        else if(parent != null)
            return parent.find(id);
        else
            throw new UndeclaredIdentifierException(id);
    }
}
