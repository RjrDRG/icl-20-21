public interface IValue { 
  String toString();
  boolean equals(IValue other);
}