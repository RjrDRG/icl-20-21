import exceptions.TypeErrorException;

public class ASTWhile implements ASTNode {

    ASTNode b;
    ASTNode i;

    public IValue eval(Environment<IValue> e) {
        IValue bv = b.eval(e);

        if(bv instanceof VBool) {
            IValue v = new VEmpty();
            while ( ((VBool)bv).getval() ) {
                v = i.eval(e);
                bv = b.eval(e);
            }
            return v;
        }

        throw new TypeErrorException("ASTWhile: argument is not a boolean");
    }

    public void compile(CodeBlock c, CompilerEnvironment e) {
        int iw = c.incrementWhileCount();
        
        c.emit("WhileEntry" + iw + ":");
        b.compile(c, e);
        c.emit("ifeq WhileExit" + iw);
        i.compile(c, e);
        c.emit("pop");
        c.emit("goto WhileEntry" + iw);
        c.emit("WhileExit" + iw + ":");
        (new ASTEmpty()).compile(c,e);
    }
    
    public IType typecheck(Environment<IType> e) { 
        IType t1 = b.typecheck(e);
        IType t2 = i.typecheck(e);
        if (t1 instanceof TBool) { 
            return t2;
        }
        else throw new TypeErrorException("ASTWhile: argument is not a boolean");
    }

    public ASTWhile(ASTNode b, ASTNode i) {
        this.b = b;
        this.i = i;
    }
}