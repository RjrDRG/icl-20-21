import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CodeBlock {
    private List<String> code;
    private List<Frame> frames;
    private int whileCounter;
    private int ifCounter;

    public CodeBlock() {
        code = new ArrayList<>();
        frames = new ArrayList<>();
        whileCounter = 0;
        ifCounter = 0;
    }

    public void emit(String bytecode) {
        code.add(bytecode);
    }

    public void save(Frame frame) {
        frames.add(frame);
    }

    public int getCurrentLineNumber() {
        return code.size() - 1;
    }

    public String getCode() {
        StringBuilder result = new StringBuilder();

        for(String cmd : code) {
            result.append(cmd).append(System.lineSeparator());
        }

        return result.toString();
    }

    public String getFrames() {
        StringBuilder result = new StringBuilder();

        for(Frame frame : frames) {
            result.append(frame.getCode()).append(System.lineSeparator()).append(System.lineSeparator());
        }

        return result.toString();
    }
    
    public String getObjects(){
        StringBuilder result = new StringBuilder();

        result.append(RefInt.getCode()).append(System.lineSeparator()).append(System.lineSeparator());
        result.append(RefBool.getCode()).append(System.lineSeparator()).append(System.lineSeparator());
        
        return result.toString();
    }
    
    public int incrementWhileCount(){
        return whileCounter++;
    }
    
    public int incrementIfCount(){
        return ifCounter++;
    }

    public void toFile() throws IOException {

        File template = new File("jasminMainTemplate.j");

        String template_content = "";

        BufferedReader reader = new BufferedReader(new FileReader(template));

        String line = reader.readLine();

        while(line != null) {
            template_content = template_content + line + System.lineSeparator();
            line = reader.readLine();
        }

        reader.close();

        String bytecodes = getCode();

        String content = template_content.replaceFirst("BYTECODES", bytecodes);

        FileWriter writer = new FileWriter("result/main.j");
        writer.write(content);

        writer.close();

        for(Frame frame : frames) {
            String frame_content = frame.getCode();
            writer = new FileWriter("result/" + frame.getName() + ".j");
            writer.write(frame_content);
            writer.close();
        }

        writer = new FileWriter("result/ref_int.j");
        writer.write(RefInt.getCode());
        writer.close();

        writer = new FileWriter("result/ref_bool.j");
        writer.write(RefBool.getCode());
        writer.close();
    }
}