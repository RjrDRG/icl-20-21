
.class public Main
.super java/lang/Object

;standard initializer

.method public <init>()V
    aload_0
    invokenonvirtual java/lang/Object/<init>()V
    return
.end method

.method public static main([Ljava/lang/String;)V

    ; set limits used by this method
    .limit locals 2
    .limit stack 256

    ; setup local variables:
    ; 0 - spaghetti stack data structure
    ; 1 - result

    ; place your bytecodes here:

    ; START

new frame_0_0
dup
invokespecial frame_0_0/<init>()V
dup
aload_0
putfield frame_0_0/sl Ljava/lang/Object;
dup
astore_0
dup
new ref_int
dup
invokespecial ref_int/<init>()V
dup
sipush 5666
putfield ref_int/value I
putfield frame_0_0/m Ljava/lang/Object;
pop
WhileEntry0:
aload_0
getfield frame_0_0/m Ljava/lang/Object;
checkcast ref_int
getfield ref_int/value I
sipush 1
if_icmpgt L25
iconst_0
goto L27
L25:
iconst_1
L27:
ifeq WhileExit0
sipush 2
aload_0
getfield frame_0_0/m Ljava/lang/Object;
checkcast ref_int
getfield ref_int/value I
sipush 2
idiv
imul
aload_0
getfield frame_0_0/m Ljava/lang/Object;
checkcast ref_int
getfield ref_int/value I
if_icmpeq L44
iconst_0
goto L46
L44:
iconst_1
L46:
ifeq IfElse0
aload_0
getfield frame_0_0/m Ljava/lang/Object;
checkcast ref_int
getfield ref_int/value I
sipush 2
idiv
dup
aload_0
getfield frame_0_0/m Ljava/lang/Object;
checkcast ref_int
swap
putfield ref_bool/value I
goto IfExit0
IfElse0:
sipush 3
aload_0
getfield frame_0_0/m Ljava/lang/Object;
checkcast ref_int
getfield ref_int/value I
imul
sipush 1
iadd
dup
aload_0
getfield frame_0_0/m Ljava/lang/Object;
checkcast ref_int
swap
putfield ref_bool/value I
IfExit0:
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
aload_0
getfield frame_0_0/m Ljava/lang/Object;
checkcast ref_int
getfield ref_int/value I
invokevirtual java/io/PrintStream/println(I)V
pop
goto WhileEntry0
WhileExit0:
iconst_0
aload_0
getfield frame_0_0/sl Ljava/lang/Object;
astore_0


    ; END

    istore_1
    getstatic java/lang/System/out Ljava/io/PrintStream;
    iload_1
    invokevirtual java/io/PrintStream/println(I)V

    return

.end method
